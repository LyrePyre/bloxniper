#!/bin/bash

IFS=$'\n'
set -euo pipefail

interval=${interval:-15}
max=${max:-3600}

declare -i minutes=0

# Detect if returning to a previous autotagged session,
# and pick up where we left off:
if (git tag -l '0m' | grep . &>/dev/null); then
  (( minutes  = $(git log -1 --format='%ct' 'origin/main') ))
  (( minutes -= $(git log -1 --format='%ct' '0m') ))
  (( minutes /= 60 ))
  # Sleep extra if this first minute tag already exists
  if (git tag -l "${minutes}m" | grep . &>/dev/null); then
    (( minutes += interval ))
    sleep $(( interval * 60 ))
  fi
fi

while [[ $minutes -lt $max ]] ; do
  git tag -a "${minutes}m" -m "AUTOTAG: $minutes minutes since start."
  git push origin main "${minutes}m"
  (( minutes += interval ))
  sleep $(( interval * 60 ))
done
