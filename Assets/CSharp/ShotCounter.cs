// Author:   Levi Perez (levianperez@gmail.com)
// Date:     2024-03-27

using UnityEngine;

using Ore;

using TMPro;


namespace Bloxniper {

[RequireComponent(typeof(RectTransform))]
public class ShotCounter : OSingleton<ShotCounter>
{
    public const int Infinity = int.MaxValue;

    public static bool CanShoot => Current.m_ShotsTaken < Current.m_ShotsAllowed;

    public static int ShotsTaken
    {
        get => IsActive ? Current.m_ShotsTaken : 0;
        set
        {
            if (!IsActive)
                return;
            Current.m_ShotsTaken = value;
            Current.UpdateText();
        }
    }

    public static int ShotsAllowed
    {
        get => IsActive ? Current.m_ShotsAllowed : 0;
        set
        {
            if (!IsActive)
                return;
            Current.m_ShotsAllowed = value < 0 ? Infinity : value;
            Current.UpdateText();
        }
    }

    public static void ResetCounter(int shotsAllowed = Infinity)
    {
        if (!IsActive)
            return;
        Current.m_ShotsTaken   = 0;
        Current.m_ShotsAllowed = Infinity;
        Current.UpdateText();
    }


    [SerializeField, HideInInspector]
    TMP_Text m_Text;

    [System.NonSerialized]
    int m_ShotsTaken   = 0,
        m_ShotsAllowed = Infinity;

    protected override void OnValidate()
    {
        base.OnValidate();

        if (!m_Text)
            m_Text = GetComponentInChildren<TMP_Text>();
    }

    protected override void OnEnable()
    {
        if (!TryInitialize(this))
            return;
        UpdateText();
    }

    void UpdateText()
    {
        if (!Application.IsPlaying(this))
            return;

        if (m_ShotsAllowed < Infinity)
            m_Text.text = $"{m_ShotsTaken} / {m_ShotsAllowed}";
        else
            m_Text.text = $"{m_ShotsTaken} / ∞";
    }

} // end class ShotCounter
}