// Author:   Levi Perez (levianperez@gmail.com)
// Date:     2024-03-27

using Ore;

using TMPro;
using UnityEngine;


namespace Bloxniper
{

[RequireComponent(typeof(TMP_Text))]
public class DebugText : OSingleton<DebugText>
{

    public static void SetText(string text)
    {
        if (!Current)
            return;

        Current.m_Text.text = text;
    }


    [SerializeField, HideInInspector]
    TMP_Text m_Text;

    [SerializeField]
    TMP_Text m_OptionalGitStamp;

    protected override void OnValidate()
    {
        base.OnValidate();
        m_Text = GetComponent<TMP_Text>();
    }

    void Start()
    {
        if (!m_OptionalGitStamp || !GitStamp.IsGitControlledProject)
            return;

        // Unhooking this editor-side; it gives strange unicode characters atm

        m_OptionalGitStamp.text = $"{GitStamp.BuildMasterName} ({GitStamp.BuildMasterEmail})\n{GitStamp.ProjectState.Hash}";
    }

} // end class DebugText

}