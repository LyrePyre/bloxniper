// Author:   Levi Perez (levianperez@gmail.com)
// Date:     2024-03-27

using UnityEngine;

using Ore;


namespace Bloxniper {

[RequireComponent(typeof(AudioSource))]
public class MusicMan : OSingleton<MusicMan>
{
    public static void PlayVictory()
    {
        OAssert.Exists(Instance);
        Instance.Play(Instance.m_Victory);
    }

    public static void PlayFailure()
    {
        OAssert.Exists(Instance);
        Instance.Play(Instance.m_Failure);
    }


    [SerializeField, HideInInspector]
    AudioSource m_Source;

    [SerializeField]
    AudioClip m_BGM, m_Victory, m_Failure;


    protected override void OnValidate()
    {
        base.OnValidate();
        m_Source = GetComponent<AudioSource>();
    }

    void Start()
    {
        Play(m_BGM);
    }

    void Play(AudioClip clip)
    {
        if (!clip)
            return;

        m_Source.Stop();
        m_Source.clip = clip;
        m_Source.Play();
    }

} // end class MusicMan

}