// Author:   Levi Perez (levianperez@gmail.com)
// Date:     2024-03-27

using UnityEngine;
using UnityEngine.UI;

using Ore;

using System.Collections;
using System.Collections.Generic;


namespace Bloxniper
{

public class ScoreMan : OSingleton<ScoreMan>
{

    public static void NotifyBloxSpawned()
    {
        ++ s_MaxScore;
    }

    public static void NotifyScore(Blox blox)
    {
        if (!IsActive)
            return;

        Current.IncrementScore();

        // TODO more blox types
    }


    [SerializeField]
    TimeInterval m_AnimTickInterval = TimeInterval.OfMillis(100);

    [System.NonSerialized]
    readonly List<Image> m_Icons = new List<Image>();

    static int s_Score, s_MaxScore;


    void Awake()
    {
        GetComponentsInChildren(m_Icons);
            // Confirmed: this list will be in-order with the hierarchy

        if (!m_Icons.IsEmpty())
            return;

        Orator.Warn("ScoreMan needs child icons to function!", this);
        gameObject.SetActive(false);
    }

    protected override void OnDisable()
    {
        s_Score = 0;
        s_MaxScore = 0;
        base.OnDisable();
    }

    IEnumerator Start()
    {
        yield return null;

        int i = m_Icons.Count;

        OAssert.True(i > 0);

        while (i < s_MaxScore)
        {
            var clone = Instantiate(m_Icons[0].gameObject, transform);
            m_Icons.Add(clone.GetComponent<Image>());
            ++ i;
        }

        while (i --> 0)
        {
            var img = m_Icons[i];
            MarkImage(img, false);
            img.gameObject.SetActive(false);
        }

        yield return null;

        var yield = m_AnimTickInterval.Yield();

        i = s_MaxScore;

        while (i --> 0)
        {
            m_Icons[i].gameObject.SetActive(true);
            yield return yield;
        }
    }


    void IncrementScore()
    {
        if (s_Score >= s_MaxScore)
            return;

        MarkImage(m_Icons[s_Score++], true);

        if (s_Score < s_MaxScore)
            return;

        MusicMan.PlayVictory();

        var vic = transform.parent.Find("Victory"); // TODO dirty
        if (!vic)
        {
            Orator.Error("Didn't find victory object in siblings.", this);
            return;
        }

        vic.gameObject.SetActive(true);
        Reticle.Mode = Reticle.ReticleMode.Hidden;
    }


    static void MarkImage(Image img, bool mark)
    {
        img.color = mark ? Color.white : Color.black;
    }

} // end class ScoreMan

}