// Author:   Levi Perez (levianperez@gmail.com)
// Date:     2024-03-26

using UnityEngine;
using UnityEngine.InputSystem;

using Ore;

namespace Bloxniper {

public class DumbShooter : OSingleton<DumbShooter>
{

    public void Fire(Vector2 viewportPt, float variance)
    {
        if (!m_ProjectilePool)
        {
            Orator.Error("Missing projectile pool reference!", this);
            return;
        }

        if (!ShotCounter.CanShoot)
            return;

        var bullet = m_ProjectilePool.Get(m_ProjectileTTL);
        var rb = bullet.GetComponent<Rigidbody>();

        if (variance > Floats.Epsilon)
        {
            viewportPt += Random.insideUnitCircle * variance;
            viewportPt.x = viewportPt.x.Clamp01();
            viewportPt.y = viewportPt.y.Clamp01();
        }

        var dir = MainCamera.Camera.ViewportPointToRay(viewportPt);

        rb.Move(dir.origin, Quaternion.identity);
        rb.AddForce(dir.direction * m_Force, ForceMode.VelocityChange);

        ++ ShotCounter.ShotsTaken;
    }


    [SerializeField]
    PrefabPool m_ProjectilePool;
    [SerializeField]
    TimeInterval m_ProjectileTTL = TimeInterval.OfSeconds(10f);

    [SerializeField, Range(1f, 1000f)]
    float m_Force = 10f;
    [SerializeField, Range(0f, 1f)]
    float m_Variance = 0f;

    protected override void OnEnable()
    {
        if (!TryInitialize(this))
            return;
        BloxniperControls.Instance.gameplay.fire.performed += PerformFire;
    }

    protected override void OnDisable()
    {
        BloxniperControls.Instance.gameplay.fire.performed -= PerformFire;
        base.OnDisable();
        m_ProjectilePool.Clear();
    }


    void PerformFire(InputAction.CallbackContext ctx)
    {
        Fire(Reticle.ViewportPosition, m_Variance);
    }

} // end class DumbShooter

}