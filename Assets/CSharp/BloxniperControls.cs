// Author:   Levi Perez (levianperez@gmail.com)
// Date:     2024-03-26

using Ore;

using UnityEngine;
using UnityEngine.SceneManagement;


namespace Bloxniper {

public partial class BloxniperControls
{

    public static readonly BloxniperControls Instance = new BloxniperControls();

    enum TempMechanics
    {
        CenterOnly,
        CursorAim,
        Quicktime, // TODO
    }

    static TempMechanics s_Mechanics = TempMechanics.CenterOnly;


    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void EnableControls()
    {
        Instance.gameplay.restart.performed += (_) =>
        {
            SceneManager.LoadScene(ActiveScene.Scene.path, LoadSceneMode.Single);
        };

        Instance.gameplay.escape.performed += (_) =>
        {
            Application.Quit();
            #if UNITY_EDITOR
            UnityEditor.EditorApplication.ExitPlaymode();
            #endif
        };

        Instance.gameplay.cycle_mechanic.performed += (_) =>
        {
            s_Mechanics = (TempMechanics)((int)(s_Mechanics + 1) % 2);
            switch (s_Mechanics)
            {
                case TempMechanics.CenterOnly:
                    Reticle.Mode = Reticle.ReticleMode.Centered;
                    DebugText.SetText("Mode: Center Screen Only <size=30>(TAB to cycle)</size>");
                    break;
                case TempMechanics.CursorAim:
                    Reticle.Mode = Reticle.ReticleMode.Cursor;
                    DebugText.SetText("Mode: Cursor Aim <size=30>(TAB to cycle)</size>");
                    break;
            }
        };

        Instance.Enable();
    }

} // end class BloxniperControls

}