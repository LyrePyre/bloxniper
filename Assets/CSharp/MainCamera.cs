// Author:   Levi Perez (levianperez@gmail.com)
// Date:     2024-03-26

using UnityEngine;
using UnityEngine.InputSystem;

using Ore;

namespace Bloxniper {

[RequireComponent(typeof(Camera))]
public class MainCamera : OSingleton<MainCamera>
{
    public static Camera Camera => IsActive ? Current.m_Camera : Camera.main;

    public static bool CanPivot { get; set; } = true;


    [SerializeField, HideInInspector]
    Camera m_Camera;

    [SerializeField]
    Transform m_PivotTarget;
    [SerializeField]
    Vector3 m_PivotPoint;

    [SerializeField]
    Vector2 m_PivotScale = new Vector2(1f, 2f);
    [SerializeField]
    Vector2 m_PivotPitchExtent = new Vector2(-5f, 85f);


    [System.NonSerialized]
    Vector4 m_PivotArm;
    [System.NonSerialized]
    Vector2 m_PivotNormalized;


    protected override void OnValidate()
    {
        base.OnValidate();

        if (!m_Camera)
            m_Camera = GetComponent<Camera>();

        if (m_PivotTarget)
            m_PivotPoint = m_PivotTarget.position;

        transform.LookAt(m_PivotPoint);
    }

    protected override void OnEnable()
    {
        if (!TryInitialize(this))
        {
            DestroySelf();
            return;
        }

        var camPos = transform.position;
        camPos -= m_PivotPoint;
        float magn = camPos.magnitude;
        m_PivotArm = camPos / magn;
        m_PivotArm.w = magn;
    }


    void Update()
    {
        if (!CanPivot)
            return;

        var move = BloxniperControls.Instance.gameplay.move.ReadValue<Vector2>();
        if (move.ApproximatelyZero())
            return;

        (move.x,move.y) = (move.y,move.x);

        m_PivotNormalized += move * m_PivotScale * Time.deltaTime;
        m_PivotNormalized.x = m_PivotNormalized.x.Clamp01();
    }

    void FixedUpdate()
    {
        ApplyPivot();
    }


    void CalcPivotArm(out Quaternion rot, out Vector3 pivot, out Vector3 arm)
    {
        pivot = m_PivotNormalized;
        pivot.x = Mathf.Lerp(m_PivotPitchExtent.x, m_PivotPitchExtent.y, pivot.x);
        pivot.y *= 360f;

        rot = Quaternion.AngleAxis(pivot.y, Vector3.up)
            * Quaternion.AngleAxis(pivot.x, Vector3.right);

        pivot = m_PivotPoint;

        arm = rot * m_PivotArm * m_PivotArm.w;
    }

    void ApplyPivot()
    {
        CalcPivotArm(out var rot, out var pivot, out var arm);
        transform.SetPositionAndRotation(pivot + arm, rot);
    }

} // end class MainCamera

}
