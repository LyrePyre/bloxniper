// Author:   Levi Perez (levianperez@gmail.com)
// Date:     2024-03-27

using UnityEngine;

using Ore;


namespace Bloxniper
{

[RequireComponent(typeof(Rigidbody), typeof(Collider))]
public class Blox : OComponent
{
    public void Kill(bool awardPoint)
    {
        if (awardPoint)
            ScoreMan.NotifyScore(this);

        Destroy(gameObject);
    }


    [SerializeField]
    float m_MinDestructiveForce = 100f;

    void OnEnable()
    {
        ScoreMan.NotifyBloxSpawned();
    }

    void OnCollisionEnter(Collision coll)
    {
        const int LAYER_GROUND     = 3;
        const int LAYER_PROJECTILE = 7;

        if (coll.gameObject.layer == LAYER_GROUND && coll.contactCount == 1)
        {
            // TODO different sfx for ground death?
            Kill(awardPoint: true);
            return;
        }

        if (coll.gameObject.layer != LAYER_PROJECTILE)
            return;

        float force = coll.relativeVelocity.magnitude;

        // Orator.Log($"force: {force:F1}, impulse: {coll.impulse.magnitude:F1}, relvel: {coll.relativeVelocity.magnitude:F1}");

        if (force < m_MinDestructiveForce)
            return;

        Kill(awardPoint: true);

        if (coll.gameObject.TryGetComponent(out AudioSource sfx))
        {
            sfx.Play();
        }
    }

} // end class Blox

}