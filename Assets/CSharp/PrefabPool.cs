// Author:   Levi Perez (levianperez@gmail.com)
// Date:     2024-03-26

using UnityEngine;
using UnityEngine.Pool;

using System.Collections;

using Ore;


namespace Bloxniper {

[CreateAssetMenu(menuName = "BLOXNIPER/PrefabPool")]
public class PrefabPool : OAsset
{

    [SerializeField]
    GameObject m_Prefab;


    [System.NonSerialized]
    ObjectPool<GameObject> m_Pool;


    void Awake()
    {
        m_Pool = new ObjectPool<GameObject>(
            createFunc: MakeNew,
            actionOnGet: OnReuse,
            actionOnRelease: Despawn,
            actionOnDestroy: Destroy,
            collectionCheck: Consts.IsDebug,
            defaultCapacity: 10,
            maxSize: 100
        );
    }


    public GameObject Get()
    {
        OAssert.Exists(m_Prefab);

        if (m_Pool is null)
            Awake();

        return m_Pool.Get();
    }

    public GameObject Get(TimeInterval ttl)
    {
        OAssert.Exists(m_Prefab);

        if (m_Pool is null)
            Awake();

        var obj = m_Pool.Get();

        if (ttl < TimeInterval.Epsilon)
            return obj;

        ActiveScene.Coroutines.Run(DieLater(obj, ttl), obj);

        return obj;
    }

    public void Release(GameObject obj)
    {
        ActiveScene.Coroutines.Halt(obj);
        m_Pool.Release(obj);
    }

    public void Clear()
    {
        m_Pool?.Clear();
    }


    GameObject MakeNew()
    {
        return Instantiate(m_Prefab);
    }

    void OnReuse(GameObject obj)
    {
        obj.SetActive(true);
        if (obj.TryGetComponent(out Rigidbody rb))
            rb.Sleep();
    }

    void Despawn(GameObject obj)
    {
        obj.SetActive(false);
    }

    IEnumerator DieLater(GameObject obj, TimeInterval ttl)
    {
        yield return ttl.Yield();
        if (obj)
            m_Pool.Release(obj);
    }

} // end class ProjectilePool

}