// Author:   Levi Perez (levianperez@gmail.com)
// Date:     2024-03-27

using UnityEngine;
using UnityEngine.UI;

using Ore;


namespace Bloxniper
{

[RequireComponent(typeof(Image))]
public class Reticle : OSingleton<Reticle>
{
    public enum ReticleMode
    {
        Hidden,
        Centered,
        Cursor,
    }


    public static ReticleMode Mode
    {
        get => IsActive ? Current.m_Mode : ReticleMode.Hidden;
        set { if (IsActive) Current.SetMode(value); }
    }

    public static Vector2 ViewportPosition => IsActive ? Current.m_Pointer
                                                       : new Vector2(0.5f, 0.5f);


    [SerializeField]
    ReticleMode m_Mode = ReticleMode.Centered;
    [SerializeField, HideInInspector]
    Image m_Reticle;
    [SerializeField, HideInInspector]
    RectTransform m_Canvas;

    [System.NonSerialized]
    Vector2 m_Pointer = new Vector2(0.5f, 0.5f);


    protected override void OnValidate()
    {
        base.OnValidate();
        m_Reticle = GetComponent<Image>();
        m_Canvas  = transform.root as RectTransform;
        SetMode(m_Mode);
    }

    protected override void OnEnable()
    {
        if (!TryInitialize(this))
            return;
        SetMode(m_Mode);
    }

    void Update()
    {
        if (m_Mode != ReticleMode.Cursor)
            return;

        var look = BloxniperControls.Instance.gameplay.look.ReadValue<Vector2>();
        var szd = m_Canvas.sizeDelta * m_Canvas.localScale;

        m_Pointer += look / szd;
        m_Pointer.x = m_Pointer.x.Clamp01();
        m_Pointer.y = m_Pointer.y.Clamp01();

        transform.position = m_Pointer * szd;
    }

    void SetMode(ReticleMode mode)
    {
        if (!Application.IsPlaying(this))
            return;

        m_Pointer = new Vector2(0.5f, 0.5f);

        switch (mode)
        {
            case ReticleMode.Hidden:
                m_Reticle.enabled = false;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.Confined;
                break;
            case ReticleMode.Centered:
                m_Reticle.enabled = true;
                transform.localPosition = new Vector3();
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                break;
            case ReticleMode.Cursor:
                m_Reticle.enabled = true;
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                break;
        }

        m_Mode = mode;
    }

    Vector2 ViewportToUI(Vector2 viewport)
    {
        var szd = m_Canvas.sizeDelta * m_Canvas.localScale;
        return new Vector2(
            viewport.x.Clamp01() * szd.x,
            viewport.y.Clamp01() * szd.y
        );
    }

    Vector2 UIToViewport(Vector2 ui)
    {
        var result = ui / m_Canvas.sizeDelta * m_Canvas.localScale;
        result.x = result.x.Clamp01();
        result.y = result.y.Clamp01();
        return result;
    }

} // end class Reticle

}