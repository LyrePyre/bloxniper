# BLOXNIPER

(a practice clone of _Boom Blox_ on PC)

![screenshot of v0.2](Media/v0.2.png)

## Evaluating this repository

I wrote [a small Bash script](autotag.sh) to handle automatically tagging the state of
this exercise every 15 minutes.

Thus, you'll find two types of tags here:
1. Auto-tags, which take the form `{minutes}m` starting at [0m](../../tags/0m). These are useful for judging how long it took me to do certain parts; I suggest using any sort of [graph view of the repo](../../network/main) for maximum effect.
2. Version tags, of which there are two:
   - [v0.1](../../releases/v0.1) - this is all I could do in the first 3 hours. Deeply unsatisfying, but playable.
   - [v0.2](../../releases/v0.2) - this is where I'm stopping. It represents roughly 5 additional hours of work.
               
## Builds

**Pre-made builds for Windows (x64) can be found in [the "Releases" tab](../../releases).**
> The `.exe` binaries available are simply self-extracting archives made using 7zip.
> World's most headache-free installer stand-in! :grinning:

If you need to build from source, all you need to do is clone this repo and build it with
**Unity 2022.3.22f1** (the latest LTS at the time of writing).  There are no weird middle
steps to worry about, to my knowledge.


## Controls

- **WASD / Arrow Keys** = Pivot the camera around the stage
- **Left-Click** = Shoot
- **TAB** = Cycle mechanics modes (currently 2 modes)
- **R** = Restart the level
- **ESC** = Exit the game immediately
- **SPACE** = Take aim (for unfinished mechanic variants)

> NOTE: I elected to use the InputSystem for these prototypes, as I understand it
> is the primary input handler used for XR device support.  I usually don't bother
> when I prototype, but I did this time to show that I can.


## Asset Credits
- I created all audio clips using Mixcraft 8.
- I created all 3D assets with ProBuilder.
- I created all UI sprites using Gimp.
- Font = Liberation Sans